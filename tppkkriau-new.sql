-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1build0.15.04.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 26, 2018 at 11:34 AM
-- Server version: 10.0.23-MariaDB-0ubuntu0.15.04.1
-- PHP Version: 5.6.4-4ubuntu6.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tppkkriau`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`id` int(15) NOT NULL,
  `title` varchar(225) NOT NULL,
  `dateadd` datetime NOT NULL,
  `permalink` varchar(225) NOT NULL,
  `datemodified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `dateadd`, `permalink`, `datemodified`) VALUES
(1, 'Uncategories', '2018-04-13 07:54:17', 'uncategories', '2018-04-26 07:38:52'),
(2, 'Kegiatan Umum', '2018-04-26 07:38:20', 'kegiatan-umum', '2018-04-26 07:38:47');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
`id` bigint(15) NOT NULL,
  `postid` bigint(15) NOT NULL,
  `comments` text NOT NULL,
  `commentstatus` varchar(15) NOT NULL,
  `commentname` varchar(50) NOT NULL,
  `commentemail` varchar(50) NOT NULL,
  `commenturl` varchar(100) NOT NULL,
  `commentcontent` text NOT NULL,
  `commentdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mediafiles`
--

CREATE TABLE IF NOT EXISTS `mediafiles` (
`id` bigint(15) NOT NULL,
  `filename` varchar(225) NOT NULL,
  `filetype` varchar(10) NOT NULL,
  `filepath` varchar(225) NOT NULL,
  `dateadd` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mediafiles`
--

INSERT INTO `mediafiles` (`id`, `filename`, `filetype`, `filepath`, `dateadd`) VALUES
(2, 'logo-tppkk.png', 'image/png', 'assets/images/2018/04/logo-tppkk.png', '2018-04-25 09:13:58'),
(3, 'peserta-jambore-1.jpg', 'image/jpeg', 'assets/images/2018/04/peserta-jambore-1.jpg', '2018-04-26 08:04:36'),
(4, 'peserta-jambore-2.jpg', 'image/jpeg', 'assets/images/2018/04/peserta-jambore-2.jpg', '2018-04-26 08:05:12'),
(5, 'gubernur-riau-pada-acara-tppkk.jpg', 'image/jpeg', 'assets/images/2018/04/gubernur-riau-pada-acara-tppkk.jpg', '2018-04-26 08:05:18'),
(6, 'jambore-kader-pkk-provinsi-riau.jpg', 'image/jpeg', 'assets/images/2018/04/jambore-kader-pkk-provinsi-riau.jpg', '2018-04-26 08:23:07'),
(7, 'jambore-kader-pkk-provinsi-riau1.jpg', 'image/jpeg', 'assets/images/2018/04/jambore-kader-pkk-provinsi-riau1.jpg', '2018-04-26 08:23:31'),
(8, 'jambore-kader-pkk-provinsi-riau.jpg', 'image/jpeg', 'assets/images/2018/04/jambore-kader-pkk-provinsi-riau.jpg', '2018-04-26 08:25:15'),
(9, 'kampung-kb-2.jpg', 'image/jpeg', 'assets/images/2018/04/kampung-kb-2.jpg', '2018-04-26 08:29:56'),
(10, 'kampung-kb-3.jpg', 'image/jpeg', 'assets/images/2018/04/kampung-kb-3.jpg', '2018-04-26 08:30:01'),
(11, 'kampung-kb.jpg', 'image/jpeg', 'assets/images/2018/04/kampung-kb.jpg', '2018-04-26 08:30:49'),
(12, 'logo-tppkk3.png', 'image/png', 'assets/images/2018/04/logo-tppkk3.png', '2018-04-26 09:07:58');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
`id` int(15) NOT NULL,
  `menuname` varchar(100) NOT NULL,
  `parent` int(15) NOT NULL,
  `permalink` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menuname`, `parent`, `permalink`) VALUES
(1, 'Profil TP PKK Provinsi Riau', 0, ''),
(2, 'Visi', 1, ''),
(3, 'Misi', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
`id` bigint(20) NOT NULL,
  `dateadd` datetime NOT NULL,
  `author` bigint(20) NOT NULL,
  `posttype` varchar(15) NOT NULL,
  `title` text NOT NULL,
  `category` int(15) NOT NULL,
  `content` longtext NOT NULL,
  `postimage` varchar(225) DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `allowcomment` varchar(10) NOT NULL,
  `permalink` text NOT NULL,
  `datemodified` datetime NOT NULL,
  `commentcount` int(15) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `dateadd`, `author`, `posttype`, `title`, `category`, `content`, `postimage`, `status`, `allowcomment`, `permalink`, `datemodified`, `commentcount`) VALUES
(2, '2018-04-25 09:48:36', 1, 'page', 'Visi', 0, '<p>Visi TP PKK Riau</p>', 'assets/images/2018/04/logo-tppkk1.png', 'Open', '', 'visi', '0000-00-00 00:00:00', 0),
(3, '2018-04-25 09:48:52', 1, 'page', 'Misi', 0, '<p>Misi TP PKK Riau</p>', 'assets/images/2018/04/logo-tppkk2.png', 'Open', '', 'misi', '0000-00-00 00:00:00', 0),
(4, '2018-04-26 08:25:15', 1, 'post', 'JAMBORE KADER TP. PKK TINGKAT PROVINSI RIAU', 0, '<p>Jambore Kader PKK adalah<br>\r\nmerupakan agenda tahunan<br>\r\nkegiatan TP, PKK secara berjen-<br>\r\njang, mulai dari tingkat kabupaten,<br>\r\nprovinsii, sampai tingkat nasional.<br>\r\nJambore PKK adalah pertemuan<br>\r\nsejumlah besar Pengurus dan<br>\r\nkader PKK, disamping mengada-<br>\r\nkan berbagai lomba antar kader,<br>\r\njuga merupakan persiapan untuk<br>\r\nmengikuti jamboree di tingkat<br>\r\nnasional. Disamping itu juga dapat<br>\r\nmenjadikan wadah untuk saling<br>\r\nberbagi pengetahuan dan pen-<br>\r\ngalaman antara sesama pengurus<br>\r\ndan kader PKK yang telah berhasil<br>\r\nmemotivasi dan menggerakkan<br>\r\nmasyarakat khususnya keluar-<br>\r\nga di daerahnya, serta kiat-kiat<br>\r\nkeberhasilannya kepada sesama<br>\r\npengurus dan kader PKK, sehingga<br>\r\nkinerja dan prestasi kader-kaser<br>\r\nPKK semakin meningkat dalam<br>\r\nmelaksanakan kegiatan di daer-<br>\r\nahnya masing-masing.</p>\r\n\r\n<p><img alt="" src="http://localhost/tppkk/assets/images/2018/04/gubernur-riau-pada-acara-tppkk.jpg" xss=removed></p>\r\n\r\n<p><img alt="" src="http://localhost/tppkk/assets/images/2018/04/peserta-jambore-2.jpg" xss=removed></p>', 'assets/images/2018/04/jambore-kader-pkk-provinsi-riau.jpg', 'Open', 'No', 'jambore-kader-tp.-pkk-tingkat-provinsi-riau', '0000-00-00 00:00:00', 0),
(5, '2018-04-26 08:30:49', 1, 'post', 'Pencanangan PKK KB Kesesehatan di Kabupaten Pelalawan dan Bengkalis', 0, '<p>Pencanangan PKK KB Keseha-<br>\r\ntan, merupakan agenda tahunan<br>\r\nsebagai momen untuk memban-<br>\r\ngkitkan semangat dan meme-<br>\r\nlihara agara gerakan Keluarga<br>\r\nBerencana dan kesehatan tetap<br>\r\nmenggema ditengah masyarakat.<br>\r\nKegiatan ini juga sengaja dilak-<br>\r\nsanakan bersempena dengan per-<br>\r\ningatan hai ulangtahun atau Hari<br>\r\nKesatuan gerak PKK ke 45 tingkat<br>\r\nProvinsi Riau yang dipusatkan di<br>\r\nKecamatan Langgam Kabupaten<br>\r\nPelalawan.<br>\r\nPencanangan KB Kesehatan<br>\r\nTingkat Provinsi Riau in disamp-<br>\r\ning dihadiri oleh Gubernur Riau,<br>\r\nyang diwakili oleh .Kepala Dinas<br>\r\nKependudukan, Catatan Sipil dan<br>\r\nKeluarga Berencana, Bupati Pelal-<br>\r\nawan M. Harris, Pejabat Pemda<br>\r\nRiau dan Kabupaten Pelalawan,<br>\r\nKepala BKKBN Riau, Kepala Dinas<br>\r\n10<br>\r\nPemberdayaan Perempuan dan<br>\r\nPerlindungazn anak Provinsi Riau,<br>\r\nKetua TP.PKK Kabupaten Pelal-<br>\r\nawan Kepala Dinas terkait baik<br>\r\ndilingkungan Pemda Riau maupun<br>\r\nKabupatern Pelalawan, dan para<br>\r\nkader PKK se Provinsi Riau<br>\r\nTujuan penyelenggaraan PKK<br>\r\nKB Kesehatan adalah untuk meng-<br>\r\ngerakkan dan mengkampanyekan<br>\r\nkembali gerakan Keluarga Beren-<br>\r\ncana ditengah masyarakat dalam<br>\r\nrangka pengendalian angka kelahi-<br>\r\nran serta menekan angka kemtian<br>\r\nIbu dan bayi untuk mewujudkan<br>\r\nsumber daya manusia Indonesia<br>\r\nyang berkualitas melalui keluarga<br>\r\nyang sangat sejahtera dan mandi-<br>\r\nri., dengan    <br>\r\nTema PKK KB<br>\r\nKesehatan ini adalah “ Melalui Bu-<br>\r\nlan Bhakti PKK KB Kesehatan ke 45<br>\r\nTahun 2017, kita ciptakan kinerja<br>\r\ngerakan PKK untuk mewujudkan<br>\r\nkeluarga berkualitas”.<br>\r\nRangkaian kegiatan yang telah,<br>\r\nsedang dan akan dilaksanakan<br>\r\nbaik oleh Provinsi maupun oleh<br>\r\nKabupaten Pelalawan selaku tuan<br>\r\nrumah adalah: Pelayanan KB dan<br>\r\nPap Smear, pengobatan masal<br>\r\nbagi masyarakat,, gerakan makan<br>\r\nsayur dan buah. bazar, gerakan<br>\r\nbugar dengan jamu (Bude Jamu),<br>\r\nlomba Posyand, lomba PKK KB<br>\r\nKesehatan, lomba PHBS, dan pen-<br>\r\ncanangan PKK KB Kesehatan oleh<br>\r\nGubernur Riau..<br>\r\nHal ini dikemukakan oleh Ketua<br>\r\nTP. PKK Provinsi Riau, pada acara<br>\r\npenyelenggaraan Pencanangan<br>\r\nPKK KB Kesehatan Tingkat Provin-<br>\r\nsi Riau di Desa Seganti Kecamatan<br>\r\nLanggam Kabupaten Perlalawan<br>\r\nbelum lama ini.<br>\r\nAcara yang sama juga telah<br>\r\ndilaksanakan di Kecamatan<br>\r\nMandau Kabupaten Bengkalis,</p>\r\n\r\n<p><img alt="" src="http://localhost/tppkk/assets/images/2018/04/kampung-kb-2.jpg" xss=removed></p>\r\n\r\n<p><img alt="" src="http://localhost/tppkk/assets/images/2018/04/kampung-kb-3.jpg" xss=removed></p>', 'assets/images/2018/04/kampung-kb.jpg', 'Open', 'No', 'pencanangan-pkk-kb-kesesehatan-di-kabupaten-pelalawan-dan-bengkalis', '0000-00-00 00:00:00', 0),
(6, '2018-04-26 09:07:58', 1, 'page', 'Profil TP PKK Provinsi Riau', 0, '<p>Profil</p>', 'assets/images/2018/04/logo-tppkk3.png', 'Open', '', 'profil-tp-pkk-provinsi-riau', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
`id` int(15) NOT NULL,
  `metakey` varchar(225) NOT NULL,
  `metavalue` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `metakey`, `metavalue`) VALUES
(1, 'site_name', 'TP PKK Provinsi Riau'),
(2, 'site_url', 'http://localhost/tppkk/'),
(3, 'site_description', 'TP PKK - Riau'),
(4, 'site_meta_keyword', 'TP PKK - Riau'),
(5, 'site_meta_description', 'TP PKK - Riau'),
(6, 'site_logo', 'assets/images/2018/04/logo-tppkk.png'),
(7, 'site_favico', ''),
(8, 'site_title', 'TP PKK Provinsi Riau'),
(9, 'site_email', 'admin@tppkk.riau.go.id'),
(10, 'site_facebook', '@tppkkriau'),
(11, 'site_instagram', '@tppkkriau'),
(12, 'site_twitter', '@tppkkriau'),
(13, 'site_autor', 'TP PKK'),
(14, 'site_limit_slider', '5'),
(15, 'site_limit_posts', '10');

-- --------------------------------------------------------

--
-- Table structure for table `userdetails`
--

CREATE TABLE IF NOT EXISTS `userdetails` (
`id` bigint(20) NOT NULL,
  `userid` bigint(20) NOT NULL,
  `metakey` varchar(100) NOT NULL,
  `metavalue` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(15) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `useremail` varchar(100) NOT NULL,
  `displayname` varchar(100) NOT NULL,
  `registered` datetime NOT NULL,
  `userstatus` varchar(10) NOT NULL,
  `activationkey` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `useremail`, `displayname`, `registered`, `userstatus`, `activationkey`) VALUES
(1, 'admintppkk', '90493ed1c297ba8282a08aea18fdf3254571b4c3', 'admin@tppkk.riau.go.id', 'Admin TPPKK', '2018-04-14 14:28:29', 'Admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE IF NOT EXISTS `visitors` (
`id` bigint(25) NOT NULL,
  `postid` bigint(15) NOT NULL,
  `datevisit` datetime NOT NULL,
  `ipaddress` varchar(16) NOT NULL,
  `useragent` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mediafiles`
--
ALTER TABLE `mediafiles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userdetails`
--
ALTER TABLE `userdetails`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `id` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
MODIFY `id` bigint(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mediafiles`
--
ALTER TABLE `mediafiles`
MODIFY `id` bigint(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
MODIFY `id` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
MODIFY `id` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `userdetails`
--
ALTER TABLE `userdetails`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
MODIFY `id` bigint(25) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
