<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class SiteConfig{
    
    public function __construct() {
        
    }
    function config(){
        $CI =& get_instance();
        $CI->load->model('Settings_model');
        foreach($CI->Settings_model->get_all() as $s){
            $CI->config->set_item($s->metakey, $s->metavalue);
        }
    }
}