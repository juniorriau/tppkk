<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('Categories_model');
        $this->load->model('Posts_model');
        $this->load->model('Menu_model');
        $this->load->library('form_validation');
        $this->load->helper('security');
    }
	public function index()
	{
        $tparent=$this->Menu_model->get_parent();
        $menu=array();
        $cat=$this->Categories_model->get_all();
        foreach($tparent as $p){
            if(empty(($this->Menu_model->get_child($p->id)))){
                $menu[$p->menuname]=array(
                    "id"=>$p->id,
                    "title"=>$p->menuname,
                    "permalink"=>$this->Menu_model->check_is_category($p->permalink),
                );
            }else{
                $child=array();
                $tchild=$this->Menu_model->get_child($p->id);
                foreach($tchild as $c){
                    $child[$c->menuname]= array(
                        "id"=>$c->id,
                        "title"=>$c->menuname,
                        "permalink"=>$this->Menu_model->check_is_category($c->permalink),
                    );
                }
                $menu[$p->menuname]=array(
                    "id"=>$p->id,
                    "title"=>$p->menuname,
                    "permalink"=>$this->Menu_model->check_is_category($p->permalink),
                    "child"=>$child,
                );
            }
        }
        $postrecent=$this->Posts_model->get_limit_data($this->config->item('site_limit_posts'), $start = 0, $q = NULL, 'post');
        $postcontent=$this->Posts_model->get_page_by_slug($this->uri->segment(2));
        $data = array(
            'menu'=>$menu,
            'content'=>$postcontent,
            'recent'=>$postrecent,
            'category'=>$cat,
            'template'=>"single-page",
        );
        $this->load->view("frontend/simple/base/wrapper",$data);
	}
}
