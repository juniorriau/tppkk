<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Menu_model');
        $this->load->model('Posts_model');
        $this->load->model('Categories_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('security');
        $this->load->model('Validator_model');        
        $this->Validator_model->is_logged_in();
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'admin/menu/index?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'admin/menu/index?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'admin/menu/index';
            $config['first_url'] = base_url() . 'admin/menu/index';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Menu_model->total_rows($q);
        $menu = $this->Menu_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'menu_data' => $menu,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'session'=>$this->session->logged_in,
        );
        $this->load->view('admin/menu/menu_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Menu_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'menuname' => $row->menuname,
		'parent' => $row->parent,
		'permalink' => $row->permalink,
            'session'=>$this->session->logged_in,
	    );
            $this->load->view('admin/menu/menu_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/menu'));
        }
    }

    public function create() 
    {
        $pages=$this->Posts_model->get_by_type('page');
        $cat=$this->Categories_model->get_all();
        $parent=$this->Menu_model->get_parent();
        $data = array(
            'button' => 'Create',
            'action' => site_url('admin/menu/create_action'),
	    'id' => set_value('id'),
            'pages'=>$pages,
            'cat'=>$cat,
            'parentm'=>$parent,
	    'menuname' => set_value('menuname'),
	    'parent' => set_value('parent'),
	    'permalink' => set_value('permalink'),
            'session'=>$this->session->logged_in,
	);
        $this->load->view('admin/menu/menu_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'menuname' => $this->input->post('menuname',TRUE),
		'parent' => $this->input->post('parent',TRUE),
		'permalink' => str_replace(" ","-",strtolower($this->input->post('menuname',TRUE))),
	    );

            $this->Menu_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('admin/menu'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Menu_model->get_by_id($id);
        $pages=$this->Posts_model->get_by_type('page');
        $cat=$this->Categories_model->get_all();
        $parent=$this->Menu_model->get_parent();
        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('admin/menu/update_action'),
		'id' => set_value('id', $row->id),
		'menuname' => set_value('menuname', $row->menuname),
		'parent' => set_value('parent', $row->parent),
                'pages'=>$pages,
            'cat'=>$cat,
            'parentm'=>$parent,
            'session'=>$this->session->logged_in,
	    );
            $this->load->view('admin/menu/menu_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/menu'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'menuname' => $this->input->post('menuname',TRUE),
		'parent' => $this->input->post('parent',TRUE),
		'permalink' => str_replace(" ","-",strtolower($this->input->post('menuname',TRUE))),
	    );

            $this->Menu_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/menu'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Menu_model->get_by_id($id);

        if ($row) {
            $this->Menu_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/menu'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/menu'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('menuname', 'menuname', 'trim|required');
	$this->form_validation->set_rules('parent', 'parent', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Menu.php */
/* Location: ./application/controllers/Menu.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-04-11 10:58:21 */
/* http://harviacode.com */
