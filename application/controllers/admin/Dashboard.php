<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Comments_model');
        $this->load->model('Posts_model');
        $this->load->model('Visitors_model');
        $this->load->library('session');
        $this->load->helper('security');
        $this->load->model('Validator_model');        
        $this->Validator_model->is_logged_in();
    }

    public function index()
    {
        $reclimit=5;
        $recpost=$this->Posts_model->get_limit_data($reclimit,0,NULL,"post");
        $postcount=$this->Posts_model->total_rows_by_type("post");
        $comcount=$this->Comments_model->total_rows();
        $visitcount=$this->Visitors_model->total_rows();
        $reccomment=$this->Comments_model->get_limit_data($reclimit,0,NULL);
        $data=array(
            'session'=>$this->session->logged_in,
            'postcount'=>$postcount,
            'comcount'=>$comcount,
            'visitcount'=>$visitcount,
            'recpost'=>$recpost,
            'reccomment'=>$reccomment,
                );
        $this->load->view("admin/dashboard",$data);
    }
}