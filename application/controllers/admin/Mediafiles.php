<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mediafiles extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mediafiles_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('security');
        $this->load->model('Validator_model');        
        $this->Validator_model->is_logged_in();
        $this->load->model('Files_model');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'admin/mediafiles/index?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'admin/mediafiles/index?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'admin/mediafiles/index';
            $config['first_url'] = base_url() . 'admin/mediafiles/index';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Mediafiles_model->total_rows($q);
        $mediafiles = $this->Mediafiles_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'mediafiles_data' => $mediafiles,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'session'=>$this->session->logged_in,
        );
        $this->load->view('admin/mediafiles/mediafiles_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Mediafiles_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'filename' => $row->filename,
		'filetype' => $row->filetype,
		'filepath' => $row->filepath,
		'dateadd' => $row->dateadd,
            'session'=>$this->session->logged_in,
	    );
            $this->load->view('admin/mediafiles/mediafiles_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/mediafiles'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('admin/mediafiles/create_action'),
	    'id' => set_value('id'),
	    'filename' => set_value('filename'),
	    'filetype' => set_value('filetype'),
	    'dateadd' => set_value('dateadd'),
            'session'=>$this->session->logged_in,
	);
        $this->load->view('admin/mediafiles/mediafiles_form', $data);
    }
    
    public function create_action() 
    {
            if ( ! $this->upload->do_upload('filename'))
            {
                $error = array('error' => $this->upload->display_errors());
                $this->create();
            }
            else
            {
                $du = $this->upload->data();
                $path=$this->Files_model->get_config();
                
                $data = array(
                    'filename' => $du['file_name'],
                    'filetype' => $du['file_type'],
                    'filepath' => $path['upload_path'].$du['file_name'],
                    'dateadd' => date("Y-m-d H:i:s"),
                     
                );
            
            
            $this->Mediafiles_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('admin/mediafiles'));
            }
    }
    
    public function update($id) 
    {
        $row = $this->Mediafiles_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('admin/mediafiles/update_action'),
		'id' => set_value('id', $row->id),
		'filename' => set_value('filename', $row->filename),
		'filetype' => set_value('filetype', $row->filetype),
		'dateadd' => set_value('dateadd', $row->dateadd),
            'session'=>$this->session->logged_in,
	    );
            $this->load->view('admin/mediafiles/mediafiles_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/mediafiles'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'filename' => $this->input->post('filename',TRUE),
		'filetype' => $this->input->post('filetype',TRUE),
		'dateadd' => $this->input->post('dateadd',TRUE),
	    );

            $this->Mediafiles_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/mediafiles'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Mediafiles_model->get_by_id($id);

        if ($row) {
            $this->Mediafiles_model->delete($id);
            unlink($row->filepath);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/mediafiles'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/mediafiles'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('filename', 'filename', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Mediafiles.php */
/* Location: ./application/controllers/Mediafiles.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-04-11 10:58:21 */
/* http://harviacode.com */
