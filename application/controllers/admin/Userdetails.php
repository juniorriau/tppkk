<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Userdetails extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Userdetails_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('security');
        $this->load->model('Validator_model');        
        $this->Validator_model->is_logged_in();
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'admin/userdetails/index?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'admin/userdetails/index?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'admin/userdetails/index';
            $config['first_url'] = base_url() . 'admin/userdetails/index';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Userdetails_model->total_rows($q);
        $userdetails = $this->Userdetails_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'userdetails_data' => $userdetails,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'session'=>$this->session->logged_in,
        );
        $this->load->view('admin/userdetails/userdetails_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Userdetails_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'userid' => $row->userid,
		'metakey' => $row->metakey,
		'metavalue' => $row->metavalue,
            'session'=>$this->session->logged_in,
	    );
            $this->load->view('admin/userdetails/userdetails_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/userdetails'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('admin/userdetails/create_action'),
	    'id' => set_value('id'),
	    'userid' => set_value('userid'),
	    'metakey' => set_value('metakey'),
	    'metavalue' => set_value('metavalue'),
            'session'=>$this->session->logged_in,
	);
        $this->load->view('admin/userdetails/userdetails_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'userid' => $this->input->post('userid',TRUE),
		'metakey' => $this->input->post('metakey',TRUE),
		'metavalue' => $this->input->post('metavalue',TRUE),
	    );

            $this->Userdetails_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('admin/userdetails'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Userdetails_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('admin/userdetails/update_action'),
		'id' => set_value('id', $row->id),
		'userid' => set_value('userid', $row->userid),
		'metakey' => set_value('metakey', $row->metakey),
		'metavalue' => set_value('metavalue', $row->metavalue),
            'session'=>$this->session->logged_in,
	    );
            $this->load->view('admin/userdetails/userdetails_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/userdetails'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'userid' => $this->input->post('userid',TRUE),
		'metakey' => $this->input->post('metakey',TRUE),
		'metavalue' => $this->input->post('metavalue',TRUE),
	    );

            $this->Userdetails_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/userdetails'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Userdetails_model->get_by_id($id);

        if ($row) {
            $this->Userdetails_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/userdetails'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/userdetails'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('userid', 'userid', 'trim|required');
	$this->form_validation->set_rules('metakey', 'metakey', 'trim|required');
	$this->form_validation->set_rules('metavalue', 'metavalue', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Userdetails.php */
/* Location: ./application/controllers/Userdetails.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-04-11 10:58:21 */
/* http://harviacode.com */
