<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Comments extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Comments_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('security');
        $this->load->model('Validator_model');        
        $this->Validator_model->is_logged_in();
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'admin/comments/index?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'admin/comments/index?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'admin/comments/index';
            $config['first_url'] = base_url() . 'admin/comments/index';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Comments_model->total_rows($q);
        $comments = $this->Comments_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'comments_data' => $comments,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'session'=>$this->session->logged_in,
        );
        $this->load->view('admin/comments/comments_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Comments_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'postid' => $row->postid,
		'comments' => $row->comments,
		'commentstatus' => $row->commentstatus,
		'commentname' => $row->commentname,
		'commentemail' => $row->commentemail,
		'commenturl' => $row->commenturl,
		'commentcontent' => $row->commentcontent,
		'commentdate' => $row->commentdate,
            'session'=>$this->session->logged_in,
	    );
            $this->load->view('admin/comments/comments_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/comments'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('admin/comments/create_action'),
	    'id' => set_value('id'),
	    'postid' => set_value('postid'),
	    'comments' => set_value('comments'),
	    'commentstatus' => set_value('commentstatus'),
	    'commentname' => set_value('commentname'),
	    'commentemail' => set_value('commentemail'),
	    'commenturl' => set_value('commenturl'),
	    'commentcontent' => set_value('commentcontent'),
	    'commentdate' => set_value('commentdate'),
            'session'=>$this->session->logged_in,
	);
        $this->load->view('admin/comments/comments_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'postid' => $this->input->post('postid',TRUE),
		'comments' => $this->input->post('comments',TRUE),
		'commentstatus' => $this->input->post('commentstatus',TRUE),
		'commentname' => $this->input->post('commentname',TRUE),
		'commentemail' => $this->input->post('commentemail',TRUE),
		'commenturl' => $this->input->post('commenturl',TRUE),
		'commentcontent' => $this->input->post('commentcontent',TRUE),
		'commentdate' => $this->input->post('commentdate',TRUE),
	    );

            $this->Comments_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('admin/comments'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Comments_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('admin/comments/update_action'),
		'id' => set_value('id', $row->id),
		'postid' => set_value('postid', $row->postid),
		'comments' => set_value('comments', $row->comments),
		'commentstatus' => set_value('commentstatus', $row->commentstatus),
		'commentname' => set_value('commentname', $row->commentname),
		'commentemail' => set_value('commentemail', $row->commentemail),
		'commenturl' => set_value('commenturl', $row->commenturl),
		'commentcontent' => set_value('commentcontent', $row->commentcontent),
		'commentdate' => set_value('commentdate', $row->commentdate),
            'session'=>$this->session->logged_in,
	    );
            $this->load->view('admin/comments/comments_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/comments'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'postid' => $this->input->post('postid',TRUE),
		'comments' => $this->input->post('comments',TRUE),
		'commentstatus' => $this->input->post('commentstatus',TRUE),
		'commentname' => $this->input->post('commentname',TRUE),
		'commentemail' => $this->input->post('commentemail',TRUE),
		'commenturl' => $this->input->post('commenturl',TRUE),
		'commentcontent' => $this->input->post('commentcontent',TRUE),
		'commentdate' => $this->input->post('commentdate',TRUE),
	    );

            $this->Comments_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/comments'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Comments_model->get_by_id($id);

        if ($row) {
            $this->Comments_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/comments'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/comments'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('postid', 'postid', 'trim|required');
	$this->form_validation->set_rules('comments', 'comments', 'trim|required');
	$this->form_validation->set_rules('commentstatus', 'commentstatus', 'trim|required');
	$this->form_validation->set_rules('commentname', 'commentname', 'trim|required');
	$this->form_validation->set_rules('commentemail', 'commentemail', 'trim|required');
	$this->form_validation->set_rules('commenturl', 'commenturl', 'trim|required');
	$this->form_validation->set_rules('commentcontent', 'commentcontent', 'trim|required');
	$this->form_validation->set_rules('commentdate', 'commentdate', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Comments.php */
/* Location: ./application/controllers/Comments.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-04-11 10:58:21 */
/* http://harviacode.com */
