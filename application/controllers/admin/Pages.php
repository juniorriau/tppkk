<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends CI_Controller
{
    public $replacereg=array(",","/","(",")","[","]","{","}","%","'"," ");
    function __construct()
    {
        parent::__construct();
        $this->load->model('Posts_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('security');
        $this->load->model('Validator_model');        
        $this->Validator_model->is_logged_in();
        $this->load->model('Files_model');
        $this->load->model('Mediafiles_model');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'admin/pages/index?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'admin/pages/index?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'admin/pages/index';
            $config['first_url'] = base_url() . 'admin/pages/index';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Posts_model->total_rows($q);
        $pages = $this->Posts_model->get_limit_data($config['per_page'], $start, $q,'page');

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        

        
        $data = array(
            'pages_data' => $pages,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'session'=>$this->session->logged_in,
        );
        $this->load->view('admin/pages/pages_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Posts_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'dateadd' => $row->dateadd,
		'author' => $row->author,
		'title' => $row->title,
		'content' => $row->content,
		'postimage' => $row->postimage,
		'status' => $row->status,
		'permalink' => $row->permalink,
		'datemodified' => $row->datemodified,
            'session'=>$this->session->logged_in
	    );
            $this->load->view('admin/pages/pages_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/pages'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('admin/pages/create_action'),
	    'id' => set_value('id'),
	    'dateadd' => set_value('dateadd'),
	    'author' => set_value('author'),
        'posttype'=> set_value('posttype'),
	    'title' => set_value('title'),
	    'content' => set_value('content'),
		'postimage' => set_value('postimage'),
	    'status' => set_value('status'),
	    'permalink' => set_value('permalink'),
	    'datemodified' => set_value('datemodified'),
            'session'=>$this->session->logged_in,
	);
        $this->load->view('admin/pages/pages_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();
        $session=$this->session->logged_in;
        $data=array();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            if ( ! $this->upload->do_upload('postimage'))
            {                
                $data = array(

                    'dateadd' => date("Y-m-d H:i:s"),
                    'author' => $session['id'],
                    'posttype' => $this->input->post('posttype',TRUE),
                    'title' => $this->input->post('title',TRUE),                    
                    'content' => $this->input->post('content',TRUE),
                    'postimage' => '',
                    'status' => $this->input->post('status',TRUE),
                    'permalink' => str_replace($this->replacereg,"-",strtolower($this->input->post('title',TRUE))),
                     
                );
            }
            else
            {
                $du = $this->upload->data();
                $path=$this->Files_model->get_config();
                $data = array(

                    'dateadd' => date("Y-m-d H:i:s"),
                    'author' => $session['id'],
                    'posttype' => $this->input->post('posttype',TRUE),
                    'title' => $this->input->post('title',TRUE),                    
                    'content' => $this->input->post('content',TRUE),
                    'postimage' => $path['upload_path'].$du['file_name'],
                    'status' => $this->input->post('status',TRUE),
                    'permalink' => str_replace($this->replacereg,"-",strtolower($this->input->post('title',TRUE))),
                     
                );
                $datau = array(
                    'filename' => $du['file_name'],
                    'filetype' => $du['file_type'],
                    'filepath' => $path['upload_path'].$du['file_name'],
                    'dateadd' => date("Y-m-d H:i:s"),
                     
                );
                $this->Mediafiles_model->insert($datau); 
            }
            
            $this->Posts_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('admin/pages'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Posts_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('admin/pages/update_action'),
		'id' => set_value('id', $row->id),
		'dateadd' => set_value('dateadd', $row->dateadd),
		'author' => set_value('author', $row->author),
		'posttype' => set_value('posttype', $row->posttype),
		'title' => set_value('title', $row->title),
		'content' => set_value('content', $row->content),
		'postimage' => $this->input->post('postimage',TRUE),
		'status' => set_value('status', $row->status),
		'allowcomment' => set_value('allowcomment', $row->allowcomment),
		'permalink' => set_value('permalink', $row->permalink),
		'datemodified' => set_value('datemodified', $row->datemodified),
		'commentcount' => set_value('commentcount', $row->commentcount),
            'session'=>$this->session->logged_in
	    );
            $this->load->view('admin/pages/pages_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/pages'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();
        $session=$this->session->logged_in;
        $data=array();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            if ( ! $this->upload->do_upload('postimage'))
            {
                $data = array(

                    'dateadd' => date("Y-m-d H:i:s"),
                    'author' => $session['id'],
                    'title' => $this->input->post('title',TRUE),
                    'content' => $this->input->post('content',TRUE),
                    'status' => $this->input->post('status',TRUE),
                    'permalink' => str_replace($this->replacereg,"-",strtolower($this->input->post('title',TRUE))),
                     
                );
            }
            else
            {
                $du = $this->upload->data();
                $path=$this->Files_model->get_config();                       
                $data = array(

                    'dateadd' => date("Y-m-d H:i:s"),
                    'author' => $session['id'],
                    'title' => $this->input->post('title',TRUE),
                    'content' => $this->input->post('content',TRUE),
                    'postimage' => $path['upload_path'].$du['file_name'],
                    'status' => $this->input->post('status',TRUE),
                    'permalink' => str_replace($this->replacereg,"-",strtolower($this->input->post('title',TRUE))),
                     
                );
                $datau = array(
                    'filename' => $du['file_name'],
                    'filetype' => $du['file_type'],
                    'filepath' => $path['upload_path'].$du['file_name'],
                    'dateadd' => date("Y-m-d H:i:s"),
                     
                );
                $this->Mediafiles_model->insert($datau); 
            }

            $this->Posts_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/pages'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Posts_model->get_by_id($id);

        if ($row) {
            $this->Posts_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/pages'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/pages'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('title', 'title', 'trim|required');
	$this->form_validation->set_rules('content', 'content', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Posts.php */
/* Location: ./application/controllers/Posts.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-04-11 10:58:21 */
/* http://harviacode.com */