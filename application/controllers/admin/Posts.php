<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Posts extends CI_Controller
{
    public $replacereg=array(",","/","(",")","[","]","{","}","%","'"," ");
    function __construct()
    {
        parent::__construct();
        $this->load->model('Posts_model');
        $this->load->model('Categories_model');
        $this->load->model('Files_model');
        $this->load->model('Mediafiles_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('security');
        $this->load->model('Validator_model');        
        $this->Validator_model->is_logged_in();
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'admin/posts/index?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'admin/posts/index?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'admin/posts/index';
            $config['first_url'] = base_url() . 'admin/posts/index';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Posts_model->total_rows('post',$q);
        $posts = $this->Posts_model->get_limit_data($config['per_page'], $start, $q,'post');

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'posts_data' => $posts,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'session'=>$this->session->logged_in,
        );
        $this->load->view('admin/posts/posts_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Posts_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'dateadd' => $row->dateadd,
		'author' => $row->author,
		'title' => $row->title,
		'content' => $row->content,
		'status' => $row->status,
		'allowcomment' => $row->allowcomment,
		'permalink' => $row->permalink,
		'datemodified' => $row->datemodified,
		'commentcount' => $row->commentcount,
            'session'=>$this->session->logged_in,
	    );
            $this->load->view('admin/posts/posts_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/posts'));
        }
    }

    public function create() 
    {
        $cat=$this->Categories_model->get_all();
        $data = array(
            'button' => 'Create',
            'action' => site_url('admin/posts/create_action'),
	    'id' => set_value('id'),
	    'dateadd' => set_value('dateadd'),
	    'author' => set_value('author'),
	    'title' => set_value('title'),
	    'category' => set_value('category'),
	    'content' => set_value('content'),
	    'status' => set_value('status'),
        'cat'=> $cat,
	    'allowcomment' => set_value('allowcomment'),
	    'permalink' => set_value('permalink'),
	    'datemodified' => set_value('datemodified'),
	    'commentcount' => set_value('commentcount'),
            'session'=>$this->session->logged_in,
	);
        $this->load->view('admin/posts/posts_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();
        $session=$this->session->logged_in;
        $data=array();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            if ( ! $this->upload->do_upload('postimage'))
            {
                $data = array(

                    'dateadd' => date("Y-m-d H:i:s"),
                    'author' => $session['id'],
                    'posttype' => $this->input->post('posttype',TRUE),
                    'title' => $this->input->post('title',TRUE),                    
                    'content' => $this->input->post('content',TRUE),
                    'category' => $this->input->post('category',TRUE),
                    'postimage' => 'assets/images/no-image.jpg',
                    'status' => $this->input->post('status',TRUE),
                    'permalink' => str_replace($this->replacereg,"-",strtolower($this->input->post('title',TRUE))),
                    'allowcomment' => $this->input->post('allowcomment',TRUE),
                    
                );
            }
            else
            {
                $du = $this->upload->data();
                $path=$this->Files_model->get_config();
                $data = array(

                    'dateadd' => date("Y-m-d H:i:s"),
                    'author' => $session['id'],
                    'posttype' => $this->input->post('posttype',TRUE),
                    'title' => $this->input->post('title',TRUE),                    
                    'content' => $this->input->post('content',TRUE),
                    'category' => $this->input->post('category',TRUE),
                    'postimage' => $path['upload_path'].$du['file_name'],
                    'status' => $this->input->post('status',TRUE),
                    'permalink' => str_replace($this->replacereg,"-",strtolower($this->input->post('title',TRUE))),
                    'allowcomment' => $this->input->post('allowcomment',TRUE),
                    
                );
                $datau = array(
                    'filename' => $du['file_name'],
                    'filetype' => $du['file_type'],
                    'filepath' => $path['upload_path'].$du['file_name'],
                    'dateadd' => date("Y-m-d H:i:s"),
                     
                );
                $this->Mediafiles_model->insert($datau);    
                
            }
            
            $this->Posts_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('admin/posts'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Posts_model->get_by_id($id);
        $cat=$this->Categories_model->get_all();
        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('admin/posts/update_action'),
		'id' => set_value('id', $row->id),
		'dateadd' => set_value('dateadd', $row->dateadd),
		'author' => set_value('author', $row->author),
		'title' => set_value('title', $row->title),
		'content' => set_value('content', $row->content),
		'postimage' => set_value('postimage', $row->postimage),
        'category'=> set_value('category', $row->category),
		'status' => set_value('status', $row->status),
		'allowcomment' => set_value('allowcomment', $row->allowcomment),
		'permalink' => set_value('permalink', $row->permalink),
		'datemodified' => set_value('datemodified', $row->datemodified),
		'commentcount' => set_value('commentcount', $row->commentcount),
            'session'=>$this->session->logged_in,
            'cat'=>$cat,    
	    );
            $this->load->view('admin/posts/posts_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/posts'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        $session=$this->session->logged_in;
        $data=array();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            if ( ! $this->upload->do_upload('postimage'))
            {
                $data = array(

                    'dateadd' => date("Y-m-d H:i:s"),
                    'author' => $session['id'],
                    'posttype' => $this->input->post('posttype',TRUE),
                    'title' => $this->input->post('title',TRUE),                    
                    'content' => $this->input->post('content',TRUE),
                    'category' => $this->input->post('category',TRUE),
                    'status' => $this->input->post('status',TRUE),
                    'permalink' => str_replace($this->replacereg,"-",strtolower($this->input->post('title',TRUE))),
                    'allowcomment' => $this->input->post('allowcomment',TRUE),
                    
                );
            }
            else
            {
                $du = $this->upload->data();
                $path=$this->Files_model->get_config();
                $data = array(

                    'dateadd' => date("Y-m-d H:i:s"),
                    'author' => $session['id'],
                    'posttype' => $this->input->post('posttype',TRUE),
                    'title' => $this->input->post('title',TRUE),                    
                    'content' => $this->input->post('content',TRUE),
                    'category' => $this->input->post('category',TRUE),
                    'postimage' => $path['upload_path'].$du['file_name'],
                    'status' => $this->input->post('status',TRUE),
                    'permalink' => str_replace($this->replacereg,"-",strtolower($this->input->post('title',TRUE))),
                    'allowcomment' => $this->input->post('allowcomment',TRUE),
                    
                );
                $datau = array(
                    'filename' => $du['file_name'],
                    'filetype' => $du['file_type'],
                    'filepath' => $path['upload_path'].$du['file_name'],
                    'dateadd' => date("Y-m-d H:i:s"),
                     
                );
                $this->Mediafiles_model->insert($datau);    
                
            }

            $this->Posts_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/posts'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Posts_model->get_by_id($id);

        if ($row) {
            $this->Posts_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/posts'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/posts'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('title', 'title', 'trim|required');
	$this->form_validation->set_rules('content', 'content', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');
	$this->form_validation->set_rules('allowcomment', 'allowcomment', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Posts.php */
/* Location: ./application/controllers/Posts.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-04-11 10:58:21 */
/* http://harviacode.com */
