<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('security');
    }
    public function index(){
        $data = array(
            'action'=> site_url('admin/auth/doauth'),
            'forgot'=> site_url('admin/auth/forgot'),
            );
        $this->load->view('admin/auth/login_form', $data);
    }
    function forgot(){
        $data = array(
            'action'=> site_url('admin/auth/doforgot'),
            'auth'=> site_url('admin/auth'),
            );
        $this->load->view('admin/auth/forgot_form', $data);
    }
    function doauth(){
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $temail=$this->input->post('email',TRUE);
            $tpassword=sha1($this->config->item('encryption_key').$this->input->post('password',TRUE));
            $data=$this->Users_model->get_user_login($temail,$tpassword);
            if($data){
                $session_data=array(
                    "id"=>$data->id,
                    "userenamil"=>$data->useremail,
                    "username"=>$data->username,
                    "displayname"=>$data->displayname,
                    "userstatus"=>$data->userstatus,
                );
                $this->session->set_userdata("logged_in",$session_data);
                redirect(site_url("admin/dashboard"));
            }
            else{
                $this->session->set_flashdata('message', 'User Not Found');
                $this->index();
            }
        }
    }
    function doforgot(){
        echo "ce;";
    }
    function logout(){
        $this->session->unset_userdata('logged_in',"");
        redirect(site_url('admin/auth'));
    }
    function _rules(){
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
    
}