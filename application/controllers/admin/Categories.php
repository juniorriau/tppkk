<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Categories_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('security');
        $this->load->model('Validator_model');        
        $this->Validator_model->is_logged_in();
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'admin/categories/index?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'admin/categories/index?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'admin/categories/index';
            $config['first_url'] = base_url() . 'admin/categories/index';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Categories_model->total_rows($q);
        $categories = $this->Categories_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'categories_data' => $categories,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'session'=>$this->session->logged_in,
        );
        $this->load->view('admin/categories/categories_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Categories_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'title' => $row->title,
		'dateadd' => $row->dateadd,
		'permalink' => $row->permalink,
		'datemodified' => $row->datemodified,
            'session'=>$this->session->logged_in,
	    );
            $this->load->view('admin/categories/categories_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/categories'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('admin/categories/create_action'),
	    'id' => set_value('id'),
	    'title' => set_value('title'),
	    'dateadd' => set_value('dateadd'),
	    'permalink' => set_value('permalink'),
	    'datemodified' => set_value('datemodified'),
            'session'=>$this->session->logged_in,
	);
        $this->load->view('admin/categories/categories_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'title' => $this->input->post('title',TRUE),
		'dateadd' => date("Y-m-d H:i:s"),
		'permalink' => str_replace(" ","-",  strtolower($this->input->post('title',TRUE))),
	    );

            $this->Categories_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('admin/categories'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Categories_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('admin/categories/update_action'),
		'id' => set_value('id', $row->id),
		'title' => set_value('title', $row->title),
		'dateadd' => set_value('dateadd', $row->dateadd),
		'permalink' => set_value('permalink', $row->permalink),
		'datemodified' => set_value('datemodified', $row->datemodified),
            'session'=>$this->session->logged_in,
	    );
            $this->load->view('admin/categories/categories_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/categories'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'title' => $this->input->post('title',TRUE),
		'permalink' => str_replace(" ","-",  strtolower($this->input->post('title',TRUE))),
		'datemodified' => date("Y-m-d H:i:s"),
	    );

            $this->Categories_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/categories'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Categories_model->get_by_id($id);

        if ($row) {
            $this->Categories_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/categories'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/categories'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('title', 'title', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Categories.php */
/* Location: ./application/controllers/Categories.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-04-11 10:58:21 */
/* http://harviacode.com */
