<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('Categories_model');
        $this->load->model('Posts_model');
        $this->load->model('Menu_model');
        $this->load->library('form_validation');
        $this->load->helper('security');
    }
	public function index()
	{
        $tparent=$this->Menu_model->get_parent();
        $menu=array();
        $cat=$this->Categories_model->get_all();
        foreach($tparent as $p){
            if(empty(($this->Menu_model->get_child($p->id)))){
                $menu[$p->menuname]=array(
                    "id"=>$p->id,
                    "title"=>$p->menuname,
                    "permalink"=>$this->Menu_model->check_is_category($p->permalink),
                );
            }else{
                $child=array();
                $tchild=$this->Menu_model->get_child($p->id);
                foreach($tchild as $c){
                    $child[$c->menuname]= array(
                        "id"=>$c->id,
                        "title"=>$c->menuname,
                        "permalink"=>$this->Menu_model->check_is_category($c->permalink),
                    );
                }
                $menu[$p->menuname]=array(
                    "id"=>$p->id,
                    "title"=>$p->menuname,
                    "permalink"=>$this->Menu_model->check_is_category($p->permalink),
                    "child"=>$child,
                );
            }
        }
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $slug=$this->uri->segment(2);
        if ($q <> '') {
            $config['base_url'] = base_url() . 'search/'.$slug.'?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'search/'.$slug.'?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'search/'.$slug.'?q=';
            $config['first_url'] = base_url() . 'search/'.$slug.'?q=';
        }

        $config['per_page'] = $this->config->item('site_limit_posts');
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Posts_model->total_rows_by_query($q,'post');
        $postcontent=$this->Posts_model->get_by_query($config['per_page'], $start, 'post',$q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        
        $postrecent=$this->Posts_model->get_limit_data($this->config->item('site_limit_posts'), $start = 0, $q = NULL, 'post');
        $data = array(
            'menu'=>$menu,
            'content'=>$postcontent,
            'recent'=>$postrecent,
            'category'=>$cat,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),            
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'template'=>"multi-post",
        );
        $this->load->view("frontend/simple/base/wrapper",$data);
	}
}
