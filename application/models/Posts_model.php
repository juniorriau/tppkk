<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Posts_model extends CI_Model
{

    public $table = 'posts';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    
    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get by post type
    function get_by_type($type){
        $this->db->where("posttype", $type);
        return $this->db->get($this->table)->result();
    }
    
    // get by permalink
    function get_by_permalink($slug){
        $this->db->where("permalink", $slug);
        return $this->db->get($this->table)->row();
    }

    // get total rows by post type
    function total_rows_by_type($type,$q = NULL){
        $this->db->where("posttype",$type);
        $this->db->group_start();
        $this->db->like('id', $q);
        $this->db->or_like('dateadd', $q);
        $this->db->or_like('author', $q);
        $this->db->or_like('title', $q);
        $this->db->or_like('content', $q);
        $this->db->or_like('status', $q);
        $this->db->or_like('allowcomment', $q);
        $this->db->or_like('permalink', $q);
        $this->db->or_like('datemodified', $q);
        $this->db->or_like('commentcount', $q);
        $this->db->group_end();
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('dateadd', $q);
	$this->db->or_like('author', $q);
	$this->db->or_like('title', $q);
	$this->db->or_like('content', $q);
	$this->db->or_like('status', $q);
	$this->db->or_like('allowcomment', $q);
	$this->db->or_like('permalink', $q);
	$this->db->or_like('datemodified', $q);
	$this->db->or_like('commentcount', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }
    function total_rows_by_author($author) {
        $this->db->where('posts.posttype','post');
        $this->db->where('users.displayname',str_replace("-"," ",$author));
        $this->db->join('users',"posts.author=users.id");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL, $type) {
        $this->db->where("posttype",$type);
        $this->db->order_by($this->id, $this->order);
        $this->db->group_start();
        $this->db->like('id', $q);
        $this->db->or_like('dateadd', $q);
        $this->db->or_like('author', $q);
        $this->db->or_like('title', $q);
        $this->db->or_like('content', $q);
        $this->db->or_like('status', $q);
        $this->db->or_like('allowcomment', $q);
        $this->db->or_like('permalink', $q);
        $this->db->or_like('datemodified', $q);
        $this->db->or_like('commentcount', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    
    function get_page_by_slug($slug)
    {
        $this->db->select('posts.*,users.displayname');
        $this->db->from($this->table);
        $this->db->where('posts.posttype', 'page');
        $this->db->where('posts.permalink', $slug);
        $this->db->join('users', 'posts.author=users.id');
        return $this->db->get()->row();
    }
    function get_post_by_slug($slug)
    {
        $this->db->select('posts.*,users.displayname, categories.title as categories');
        $this->db->from($this->table);
        $this->db->where('posts.posttype', 'post');
        $this->db->where('posts.permalink', $slug);
        $this->db->join('users', 'posts.author=users.id');
        $this->db->join('categories', 'categories.id=posts.category');
        return $this->db->get()->row();
    }
    function get_by_category($limit, $start = 0, $type, $cat)
    {
        $this->db->select('posts.*,users.displayname');
        $this->db->from($this->table);
        $this->db->where("posts.posttype",$type);
        $this->db->where("posts.status","Open");
        $this->db->where('categories.permalink', $cat);
        $this->db->join('users', 'posts.author=users.id');
        $this->db->join('categories', 'categories.id=posts.category');
        $this->db->order_by('posts.id', 'desc');
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }
    function total_rows_by_query($q,$type='post')
    {
        $this->db->select('posts.*,users.displayname, categories.title as categories ');
        $this->db->from($this->table);
        $this->db->where("posts.posttype",$type);        
        $this->db->where("posts.status","Open");
        $this->db->join('users', 'posts.author=users.id');
        $this->db->join('categories', 'categories.id=posts.category');
        $this->db->order_by('posts.id', 'desc');
        $this->db->group_start();
        $this->db->or_like('users.displayname', $q);
        $this->db->or_like('posts.title', $q);
        $this->db->or_like('categories.title', $q);
        $this->db->or_like('posts.content', $q);
        $this->db->group_end();
        return $this->db->count_all_results();
    }
    function get_by_query($limit, $start = 0, $type='post', $q){
        $this->db->select('posts.*,users.displayname, categories.title as categories ');
        $this->db->from($this->table);
        $this->db->where("posts.posttype",$type);        
        $this->db->where("posts.status","Open");
        $this->db->join('users', 'posts.author=users.id');
        $this->db->join('categories', 'categories.id=posts.category');
        $this->db->order_by('posts.id', 'desc');
        $this->db->group_start();
        $this->db->or_like('users.displayname', $q);
        $this->db->or_like('posts.title', $q);
        $this->db->or_like('categories.title', $q);
        $this->db->or_like('posts.content', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }
    function get_by_author($limit, $start = 0, $type, $author)
    {
        $this->db->select('posts.*,users.displayname');
        $this->db->from($this->table);
        $this->db->where("posts.posttype",$type);
        $this->db->where('users.displayname', str_replace("-"," ",$author));
        $this->db->join('users', 'posts.author=users.id');
        $this->db->order_by('posts.id', 'desc');
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }
    
    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Posts_model.php */
/* Location: ./application/models/Posts_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-04-11 10:58:21 */
/* http://harviacode.com */