<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Files_model extends CI_Model
{
    public $conf=array();
    
    function __construct()
    {
        parent::__construct();
        self::set_config();
        if (!file_exists('assets/images/'.date("Y")."/".date("m"))) {
                mkdir('assets/images/'.date("Y")."/".date("m"), 0755, true);
            }

            $this->load->library('upload', self::get_config());
    }
    function set_config(){
        $this->conf['upload_path'] = 'assets/images/'.date("Y")."/".date("m")."/";
        $this->conf['allowed_types']='gif|jpg|png|pdf|doc|docx';
        $this->conf['max_size']= 0;
        $this->conf['max_width']= 0;
        $this->conf['max_height']= 0;
    }
    function get_config(){
        return $this->conf;
    }
}