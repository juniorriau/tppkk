
<div class="mb25 hidden-sm hidden-xs"></div><!-- margin -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <?php if(empty($content)){
                                include 'base/404.php';
                            }else{ ?>
                                <article class="entry">
                                <div class="entry-media">
                                    <figure>
                                        <img src="<?php echo site_url($content->postimage)?>" alt="entry image">
                                    </figure>
                                </div><!-- End .enty-media -->

                                <h2 class="entry-title"><?php echo $content->title;?></h2>
                                <div class="entry-meta">
                                    <span><i class="fa fa-calendar"></i> <?php echo $content->dateadd;?></span>
                                    <a href="#"><i class="fa fa-comments"></i> <?php echo $content->commentcount;?> Comment(s)</a>
                                    <a href="<?php echo site_url("authors/".str_replace(" ","-",$content->displayname))?>"><i class="fa fa-user"></i> <?php echo $content->displayname;?></a>
                                </div><!-- End .entry-meta -->

                                <div class="entry-content">
                                    <?php echo $content->content;?>
                                </div><!-- End .entry-content -->

                                <div class="entry-author">
                                    <figure>
                                        <i class="fa fa-user-circle-o fa-2x"></i>
                                    </figure>
                                    <div class="author-content">
                                        <h4><a href="<?php echo site_url("authors/".$content->displayname)?>"> <?php echo $content->displayname;?></a></h4>
                                        
                                        <div class="social-icons">
                                            <label>Find Us:</label>
                                            <a href="#" class="social-icon" title="Facebook"><i class="fa fa-facebook"></i></a>
                                            <a href="#" class="social-icon" title="Twitter"><i class="fa fa-twitter"></i></a>
                                            <a href="#" class="social-icon" title="Github"><i class="fa fa-github"></i></a>
                                            <a href="#" class="social-icon" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                                            <a href="#" class="social-icon" title="Tumblr"><i class="fa fa-tumblr"></i></a>
                                            <a href="#" class="social-icon" title="Flickr"><i class="fa fa-flickr"></i></a>
                                        </div><!-- End .social-icons -->
                                    </div><!-- end .author-content -->
                                </div><!-- End .entry-author -->
                            </article>
                            <?php } ?>

                            
                        </div><!-- End .col-md-9 -->

                        <?php include 'sidebar.php'?>
                    </div><!-- end .row -->
                </div><!-- End .container -->
                <div class="mb10 hidden-sm hidden-xs"></div><!-- margin -->