<div class="mb60 mb50-sm"></div><!-- margin -->
<div class="main">
  <div class="container">
    <div class="row">
      <div class="col-md-9 entry">
        <?php 
          if(empty($content)){
              include 'base/404.php';
          }else{
          foreach($content as $c){ ?>
            <article class="entry entry-list">
              <div class="row">
                <div class="col-md-6">
                  <div class="entry-media">
                    <figure>
                      <a href="<?php echo site_url('post/'.$c->permalink); ?>"><img src="<?php echo site_url($c->postimage); ?>" alt="<?php echo $c->title;?>"></a>
                    </figure>
                    <div class="entry-meta">
                      <span><i class="fa fa-calendar"></i><?php echo $c->dateadd?></span>
                      <a href="<?php echo site_url('authors/'.str_replace(" ","-",$c->displayname)); ?>"><i class="fa fa-user"></i> <?php echo $c->displayname; ?></a>
                    </div><!-- End .entry-media -->
                  </div><!-- End .entry-media -->
                </div><!-- End .col-md-6 -->
                <div class="col-md-6">
                  <h2 class="entry-title"><i class="fa fa-file-image-o"></i><a href="<?php echo site_url('post/'.$c->permalink); ?>"><?php echo $c->title; ?></a></h2>
                  <div class="entry-content">
                    <p><?php echo substr($c->content, 0, 100);?></p>
                    <a href="<?php echo site_url('post/'.$c->permalink); ?>" class="readmore">Read More<i class="fa fa-angle-right"></i></a>
                  </div><!-- End .entry-content -->
                </div><!-- End .col-md-6 -->
              </div><!-- End .row -->
            </article>
          <?php }} ?>
        <nav class="pagination-container">
          <label>Showing: <?php echo count($content).' of '.$total_rows ?></label>
          <?php echo $pagination ?>
        </nav>
      </div><!-- End .col-md-9 -->
      <?php
        $this->load->view('frontend/simple/sidebar');
      ?>
    </div><!-- End .row -->
  </div><!-- End .container -->
</div><!-- End .main -->
