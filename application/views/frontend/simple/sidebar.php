<aside class="col-md-3 sidebar">
  <div class="widget search-widget">
    <h3 class="widget-title">Search</h3>
    <form action="<?php echo site_url("search/")?>" method="get">
      <input name="q" id="q" type="search" class="form-control" placeholder="Search in here" required>
    </form>
  </div><!-- End .widget -->

  <div class="widget">
    <h3 class="widget-title">Categories</h3>
    <ul class="fa-ul">
        <?php foreach($category as $cat){ ?>
        <li><a href="<?php echo site_url("category/".$cat->title)?>"><i class="fa-li fa fa-chain"></i><?php echo $cat->title?></a></li>
        <?php } ?>
    </ul>
  </div><!-- end .widget -->

  <div class="widget">
    <h3 class="widget-title">Recent Posts</h3>
    <ul class="posts-list">
      <li>
        <?php 
        foreach ($recent as $c){ ?>
            <li>
                <figure>
                    <a href="<?php echo site_url("post/".$c->permalink)?>" title="Dolores labore quod"><img src="<?php echo site_url($c->postimage)?>" alt="<?php echo $c->title?>"></a>
                </figure>
                <h5><a href="<?php echo site_url("post/".$c->permalink)?>"><?php echo $c->title?></a></h5>
                <span><?php echo $c->dateadd?></span>
            </li>
        <?php } ?>
    </ul>
  </div><!-- End .widget -->
    <div class="widget">
    <h3 class="widget-title">Twitter Feed</h3>
    <div class="twitter-feed-widget">
                                    
    </div><!-- End .twitter-feed-widget -->
    </div><!-- End .widget -->

    <div class="widget">
        <h3 class="widget-title">Facebook Feeds</h3>
        <div class="twitter-feed-widget">
                                    
        </div><!-- End .embed-responsive -->
    </div><!-- End .widget -->
</aside>
