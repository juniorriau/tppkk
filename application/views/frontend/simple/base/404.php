<div class="error-page text-center">
    <div class="container">
        <h1>404</h1>
        <h2>CONTENT NOT FOUND!</h2>
        <p>We're sorry, the content you requested cannot be found. Please go back to home page.</p>
        <a href="<?php echo site_url();?>" class="btn btn-custom2 btn-lg min-width">Back to Home</a>
    </div><!-- End .container -->
</div><!-- End .error-page -->