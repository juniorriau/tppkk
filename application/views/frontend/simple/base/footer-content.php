<footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="widget about-widget">
                                <img width="50%" src="<?php echo site_url($this->config->item('site_logo'))?>" alt="<?php echo $this->config->item('site_name');?>">
                                <hr>
                                <p>Sekretariat Pemberdayaan dan Kesejahteraan Keluarga Tim Penggerak Provinsi Riau.</p>
                                <address>
                                    <label>Get In Touch:</label> <br>
                                    Jl. Diponegoro No 36 A Pekanbaru<br><br>
                                    Telepon (0761) 23349 Fax (0761) 23349<br>
                                    <a href="mailto:#">admin@tppkk.riau.go.id</a>
                                </address>
                            </div><!-- End .widget -->
                        </div><!-- End .col-md-3 -->
                        
                        <div class="col-md-3 col-sm-6">
                            <div class="widget">
                                <h4 class="widget-title">Recent Posts</h4>
                                <ul class="posts-list">
                                    <?php 
                                    foreach ($recent as $c){ ?>
                                    <li>
                                        <figure>
                                            <a href="<?php echo site_url("post/".$c->permalink)?>" title="Dolores labore quod"><img src="<?php echo site_url($c->postimage)?>" alt="<?php echo $c->title?>"></a>
                                        </figure>
                                        <h5><a href="<?php echo site_url("post/".$c->permalink)?>"><?php echo $c->title?></a></h5>
                                        <span><?php echo $c->dateadd?></span>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div><!-- End .widget -->
                        </div><!-- End .col-md-3 -->

                        <div class="clearfix visible-sm"></div><!-- clearfix -->

                        <div class="col-md-3 col-sm-6">
                            <div class="widget">
                                <h4 class="widget-title">Tagcloud</h4>
                                <div class="tagcloud">
                                    <a href="#">TPPKK</a>
                                    <a href="#">TPPKK Provinsi Riau</a>
                                    <a href="#">TPPKK Riau</a>

                                </div><!-- End .tagcloud -->
                            </div><!-- End .widget -->
                        </div><!-- End .col-md-3 -->

                        <div class="col-md-3 col-sm-6">

                        </div><!-- End .col-md-3 -->
                            
                    </div><!-- End .row -->
                </div><!-- End .container -->

                <div class="footer-bottom">
                    <div class="container">
                        <div class="footer-right">
                            <p class="copyright"><?php echo $this->config->item("site_name");?>&copy; 2018</p>
                        </div><!-- End .footer-right -->
                        
                        <div class="social-icons">
                            <label>Find Us:</label>
                            <a href="#" class="social-icon" title="Facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="social-icon" title="Twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="social-icon" title="Github"><i class="fa fa-github"></i></a>
                            <a href="#" class="social-icon" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                            <a href="#" class="social-icon" title="Tumblr"><i class="fa fa-tumblr"></i></a>
                            <a href="#" class="social-icon" title="Flickr"><i class="fa fa-flickr"></i></a>
                            <a href="#" class="social-icon" title="Github"><i class="fa fa-github"></i></a>
                            <a href="#" class="social-icon" title="Ge"><i class="fa fa-ge"></i></a>
                            <a href="#" class="social-icon" title="Pinterest"><i class="fa fa-pinterest"></i></a>
                            <a href="#" class="social-icon" title="Amazon"><i class="fa fa-amazon"></i></a>
                            <a href="#" class="social-icon" title="Codepen"><i class="fa fa-codepen"></i></a>
                        </div><!-- End .social-icons -->
                    </div><!-- End .container -->
                </div><!-- End .footer-bottom -->
            </footer><!-- End .footer -->