<a id="scroll-top" href="#top" title="Scroll top"><i class="fa fa-angle-up"></i></a>
<!-- End -->
<script src="<?php echo base_url(); ?>themes/frontend/simple/assets/js/plugins.min.js"></script>
<script src="<?php echo base_url(); ?>themes/frontend/simple/assets/js/twitter/jquery.tweet.min.js"></script>
<script src="<?php echo base_url(); ?>themes/frontend/simple/assets/js/main.js"></script>

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="<?php echo base_url(); ?>themes/frontend/simple/assets/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>themes/frontend/simple/assets/js/jquery.themepunch.revolution.min.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS
(Load Extensions only on Local File Systems !
The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="<?php echo base_url(); ?>themes/frontend/simple/assets/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>themes/frontend/simple/assets/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>themes/frontend/simple/assets/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>themes/frontend/simple/assets/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>themes/frontend/simple/assets/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>themes/frontend/simple/assets/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>themes/frontend/simple/assets/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>themes/frontend/simple/assets/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>themes/frontend/simple/assets/js/extensions/revolution.extension.video.min.js"></script>

<script type="text/javascript">
jQuery(document).ready(function() {
    "use strict";

    var revapi;
    if ( $("#rev_slider").revolution == undefined ) {
        revslider_showDoubleJqueryError("#rev_slider");
    } else {
        revapi = $("#rev_slider").show().revolution({
            sliderType: "standard",
            jsFileLocation: "assets/js/",
            sliderLayout: "auto",
            dottedOverlay:"none",
            delay: 15000,
            navigation: {
                mouseScrollNavigation: "off",
                onHoverStop: "off",
                touch: {
                    touchenabled: "on"
                },
                arrows: {
                    style: "hermes",
                    enable: true,
                    hide_onmobile: false,
                    hide_under: 992,
                    hide_onleave: true,
                    tmp: '<div class="tp-arr-allwrapper">   <div class="tp-arr-imgholder"></div>    <div class="tp-arr-titleholder">{{title}}</div> </div>',
                    left: {
                        h_align: "left",
                        v_align: "center",
                        h_offset: 0,
                        v_offset: 0
                    },
                    right: {
                        h_align: "right",
                        v_align: "center",
                        h_offset: 0,
                        v_offset: 0
                    }
                },
                bullets: {
                    enable: false,
                    hide_onmobile: false,
                    style: "hades",
                    hide_onleave: false,
                    direction: "horizontal",
                    h_align: "right",
                    v_align: "bottom",
                    h_offset: 20,
                    v_offset: 20,
                    space: 5,
                    tmp: ''
                }
            },
            responsiveLevels: [1200,992,768,480],
            gridwidth: [1170,970,750,480],
            gridheight: [600,520,450,340],
            lazyType: "smart",
            spinner: "spinner2",
            parallax: {
                type: "on",
                origo: "slidercenter",
                speed: 2000,
                levels: [2,3,4,5,6,7,12,16,10,50],
                disable_onmobile: "on"
            },
            debugMode: false
        });
    }
});
</script>
