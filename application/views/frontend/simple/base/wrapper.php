<?php
$this->load->view("frontend/simple/base/head");
$this->load->view("frontend/simple/base/css");

?>
</head>
    <body>
        <div id="wrapper">
            <?php $this->load->view("frontend/simple/base/header"); ?>
            <div class="main">
                <?php  
                $this->load->view("frontend/simple/".$template); 
                ?>
            </div>
            
            <?php $this->load->view("frontend/simple/base/footer-content");?>
        </div>
        
<?php
$this->load->view("frontend/simple/base/js");
$this->load->view("frontend/simple/base/footer");