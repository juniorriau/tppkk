<!-- Google Fonts -->
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700%7COpen+Sans:300,300i,400,400i,600,600i,700,800" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/simple/assets/css/plugins.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/simple/assets/css/settings.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/simple/assets/css/layers.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/simple/assets/css/navigation.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/simple/assets/css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/simple/assets/css/homepages/index-medical.css">
<!-- Favicon -->
<link rel="icon" type="image/png" href="<?php echo base_url(); ?>themes/frontend/simple/assets/images/icons/favicon.png">

<!-- Modernizr -->
<script src="<?php echo base_url(); ?>themes/frontend/simple/assets/js/modernizr.js"></script>
