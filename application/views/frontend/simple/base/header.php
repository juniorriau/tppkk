 <header class="header sticky-header">
                <div class="header-top dark">
                    <div class="container">
                        <div class="social-icons hidden-xs pull-left">
                            <a href="#" class="social-icon" title="Facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="social-icon" title="Twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="social-icon" title="Github"><i class="fa fa-github"></i></a>
                            <a href="#" class="social-icon" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                            <a href="#" class="social-icon" title="Instagram"><i class="fa fa-instagram"></i></a>
                            <a href="#" class="social-icon" title="Youtube"><i class="fa fa-youtube"></i></a>
                            <a href="#" class="social-icon" title="Flickr"><i class="fa fa-flickr"></i></a>
                        </div><!-- End .social-icons -->
                    </div><!-- End .container -->
                </div><!-- End .header-top -->

                <div class="header-inner">
                    <div class="container">
                        <a href="<?php echo site_url();?>" class="site-logo" title="<?php echo site_url($this->config->item('site_name'));?>">
                            <img width="100%" class="img-responsive" src="<?php echo site_url($this->config->item('site_logo_header'));?>" alt="<?php echo site_url($this->config->item('site_name'));?>"> 
                            <span class="sr-only"><?php echo $this->config->item('site_name');?></span>
                        </a>

                    </div><!-- End .container -->
                </div><!-- End .header-inner -->

                <div class="header-bottom custom">
                    <div class="container">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-container" aria-expanded="false">
                            <span class="toggle-text">Menu</span>
                            <span class="toggle-wrapper">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </span>
                        </button>
                        <div class="collapse navbar-collapse" id="main-nav-container">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="<?php echo site_url();?>">Home</a>
                                </li>
                                <?php
                                foreach($menu as $nav){ ?>
                                <?php if(empty($nav['child'])){ ?>
                                <li class="dropdown">
                                    <a href="<?php echo base_url($nav['permalink']);?>"><?php echo $nav['title']?></a>
                                </li>
                                <?php }
                                else {?>
                                <li class="dropdown">
                                    <a href="<?php echo base_url($nav['permalink']);?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $nav['title']?><span class="angle"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <?php foreach($nav['child'] as $snav){ ?>
                                             <li><a href="<?php echo base_url($snav['permalink']);?>"><?php echo $snav['title']; ?></a></li>   
                                        <?php    } ?>
                                    </ul>
                                </li>
                                <?php } } ?>                                   
                                <li class="dropdown cart-dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="visible-inline-xs"></span><i class="fa fa-search"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                                        <form action="<?php echo site_url("search/")?>" method="get">
                                            <input type="search" name="q" id="q" class="form-control" placeholder="Search..." required>
                                            <button type="submit" class="btn btn-custom"><i class="fa fa-search"></i></button>
                                        </form>
                                    </div>
                                </li>                       
                               
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- End .container -->
                </div><!-- End .header-bottom -->
            </header><!-- End .header -->