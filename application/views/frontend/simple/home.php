<div id="rev_slider_wrapper" class="slider-container-medical rev_slider_wrapper rev_container_1 fullwidthbanner-container" data-alias="classicslider1">
                    <div id="rev_slider" class="rev_slider fullwidthabanner" style="display:none;">
                        <ul>
                            <!-- SLIDE  -->
                            <li data-index="rs-1" data-transition="fade" data-slotamount="7" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="1200" data-rotate="0" data-saveperformance="off" data-thumb="<?php echo site_url($this->config->item('site_slide_image'))?>" data-title="<?php echo $this->config->item('site_slide_text')?>">

                                <!-- MAIN IMAGE -->
                                <img src="<?php echo site_url($this->config->item('site_slide_image'))?>" alt="Slider bg 1" data-bgposition="center center" data-bgfit="cover" data-duration="11000" data-bgparallax="10" data-ease="Linear.easeNone" class="rev-slidebg" data-no-retina>
                                <!-- LAYERS -->

                                <div class="tp-caption tp-resizeme rs-parallaxlevel-0" 
                                    data-x="['center','left','left','left']" data-hoffset="['0','0','0','0']" 
                                    data-y="['center','bottom','bottom','center']" data-voffset="['20','20','20','20']" 
                                    data-width="none"
                                    data-height="none"
                                    data-fontsize="['26','24','22','20']"
                                    data-fontweight="400"
                                    data-lineheight="['36','34','32','30']"
                                    data-visibility="['on', 'on', 'on', 'off']"
                                    data-color="#000"
                                    data-whitespace="nowrap"
                                   data-frames='[{"delay":1400,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":700,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                    data-responsive_offset="on" 
                                    style="z-index: 6; text-transform: uppercase;  padding: 0.25em 0.8em; background-color: rgb(153, 204, 255,0.4)">
                                        <?php echo $this->config->item('site_slide_text')?>
                                </div>
                            </li>
                        </ul>
                        <div class="tp-bannertimer" style="height: 3px; background-color: rgba(0, 0, 0, 0.2);"></div>
                    </div><!-- End #rev_slider -->
                </div><!-- END REVOLUTION SLIDER -->

                <div class="container">

                    <div class="mb40 visible-xs visible-sm"></div><!-- margin -->

                    <div class="services-group custom">
                        <div class="service-row">
                            <div class="service service-box hidden-xs" >
                                <i class="fa fa-calendar-check"></i>
                                <p>Penghayatan dan Pengamalan PANCASILA.</p>
                            </div><!-- End .service -->

                            <div class="service service-box hidden-xs" >
                                <i class="fa fa-calendar-check"></i>
                                <p>Gotong Royong.<br></p>
                            </div><!-- End .service -->

                            <div class="service service-box hidden-xs">
                                <i class="fa fa-calendar-check"></i>
                                <p>Pangan.<br></p>
                            </div><!-- End .service -->

                            <div class="service service-box hidden-xs">
                                <i class="fa fa-calendar-check"></i>
                                <p>Sandang.<br></p>
                            </div><!-- End .service -->
                        </div><!-- End .row -->

                        <div class="service-row">
                            <div class="service service-box hidden-xs">
                                <i class="fa fa-calendar-check"></i>
                                <p>Perumahan dan Tata Laksana Rumah Tangga.</p>
                            </div><!-- End .service -->

                            <div class="service service-box hidden-xs">
                                <i class="fa fa-calendar-check"></i>
                                <p>Pendidikan dan Keterampilan.</p>
                            </div><!-- End .service -->

                            <div class="service service-box hidden-xs">
                                <i class="fa fa-calendar-check"></i>
                                <p>Kesehatan.<br></p>
                            </div><!-- End .service -->

                            <div class="service service-box hidden-xs">
                                <i class="fa fa-calendar-check"></i>
                                <p>Pengembangan Kehidupan Berkoperasi.</p>
                            </div><!-- End .service -->
                        </div><!-- End .row -->
                        
                        <div class="service-row">
                            <div class="service service-box hidden-xs" >
                                
                            </div><!-- End .service -->

                            <div class="service service-box hidden-xs">
                                <i class="fa fa-calendar-check"></i>
                                <p>Kelestarian Lingkungan Hidup.<br></p>
                            </div><!-- End .service -->

                            <div class="service service-box hidden-xs">
                                <i class="fa fa-calendar-check"></i>
                                <p>Perencanaan sehat.<br></p>
                            </div><!-- End .service -->

                            <div class="service service-box hidden-xs" >
                               
                            </div><!-- End .service -->
                        </div><!-- End .row -->

                    </div><!-- End .services-group -->
                </div><!-- End .container -->
                <div class="mb10 hidden-sm hidden-xs"></div><!-- margin -->

                <div class="container">
                    <div class="row entry">
                        <div class="col-md-6">
                            <h2 class="title custom">Sekapur Sirih</h2>
                            <p ><?php echo $this->config->item("site_sekapursirih_content");?></p>
                            <div class="mb10"></div><!-- margin -->
                        </div><!-- End .col-md-6 -->

                        <div class="clearfix mb20 visible-xs visible-sm"></div><!-- margin -->

                        <div class="col-md-6">
                            <img src="<?php echo site_url($this->config->item("site_sekapursirih_image"))?>" alt="Ketua PKK" class="img-responsive">
                        </div><!-- End .col-md-6 -->
                    </div><!-- End .row -->
                </div><!-- End .container -->
                <div class="container">
                        <div class="row">
                               
                            <div class="col-md-4 hidden-sm hidden-xs">
                                <img src="<?php echo site_url($this->config->item("site_sekapursirih_image"))?>" alt="Ketua PKK" class="img-responsive">
                            </div><!-- End .col-md-4 -->

                            <div class="col-md-8">
                                <h2 class="title custom mb25">Latest News</h2>

                                <div class="row row-sm">
                                    <?php 
                                    foreach ($recent as $c){ ?>
                                    <div class="col-sm-4">
                                        <div class="text-block hover-bg text-center" style="background-image:url(<?php echo site_url($c->postimage)?>); height:300px;">
                                            <img src="<?php echo site_url($c->postimage)?>" alt="Microscope" width="42">
                                            <h3 class="block-title"><a href="<?php echo site_url("post/".$c->permalink)?>"><?php echo $c->title?></a></h3>
                                            <p><?php echo substr($c->content, 0, 100)?></p>
                                            <a href="<?php echo site_url("post/".$c->permalink)?>" class="readmore custom2">ReadMore <i class="fa fa-angle-right"></i></a>
                                        </div><!-- End .text-block -->
                                    </div><!-- End .col-sm-4 -->
                                    <?php } ?>
                                </div><!-- End .row -->                                
                            </div><!-- End .col-md-8 -->
                            
                        </div><!-- End .row -->
                    </div><!-- End .container -->
                <!--<div class="container">
                    <h2 class="title custom mb25 entry">Latest News</h2>

                    <div class="owl-carousel latest-posts-medical-carousel">
                        <?php 
                        foreach ($recent as $c){ ?>
                        <article class="entry entry-grid">
                            <div class="entry-media">
                                <figure>
                                    <a href="<?php echo site_url("post/".$c->permalink)?>"><img src="<?php echo site_url($c->postimage)?>" alt="<?php echo $c->title?>"></a>
                                </figure>
                                <div class="entry-meta">
                                    <span><i class="fa fa-calendar"></i><?php echo $c->dateadd?></span>
                                    <!--<a href="#"><i class="fa fa-user"></i> Admin</a> 
                                </div>
                            </div>
                            <h2 class="entry-title"><i class="fa fa-file-image-o"></i><a href="<?php echo site_url("post/".$c->permalink)?>"><?php echo $c->title?></a></h2>
                            <div class="entry-content">
                                <p><?php echo substr($c->content, 0, 100)?></p>
                                <a href="<?php echo site_url("post/".$c->permalink)?>" class="readmore">Read more<i class="fa fa-angle-right"></i></a>
                            </div>
                        </article>
                        <?php } ?>

                    </div>
                </div> -->

                <div class="mb60 mb50-sm"></div><!-- margin -->