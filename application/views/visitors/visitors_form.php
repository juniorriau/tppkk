<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Visitors <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="bigint">Postid <?php echo form_error('postid') ?></label>
            <input type="text" class="form-control" name="postid" id="postid" placeholder="Postid" value="<?php echo $postid; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Datevisit <?php echo form_error('datevisit') ?></label>
            <input type="text" class="form-control" name="datevisit" id="datevisit" placeholder="Datevisit" value="<?php echo $datevisit; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Ipaddress <?php echo form_error('ipaddress') ?></label>
            <input type="text" class="form-control" name="ipaddress" id="ipaddress" placeholder="Ipaddress" value="<?php echo $ipaddress; ?>" />
        </div>
	    <div class="form-group">
            <label for="useragent">Useragent <?php echo form_error('useragent') ?></label>
            <textarea class="form-control" rows="3" name="useragent" id="useragent" placeholder="Useragent"><?php echo $useragent; ?></textarea>
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('visitors') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>