  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo site_url() ?>themes/admin/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo site_url() ?>themes/admin/assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo site_url() ?>themes/admin/assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo site_url() ?>themes/admin/assets/dist/css/skins/_all-skins.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo site_url() ?>themes/admin/assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="<?php echo site_url() ?>themes/admin/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo site_url() ?>themes/admin/assets/dist/css/custom.css">
  </head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">