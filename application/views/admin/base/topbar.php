  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo site_url() ?>admin" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><i class="fa fa-server"></i></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><?php echo $this->config->item('site_name');?></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span id="notif" class="label label-warning"></span>
            </a>
            <ul class="dropdown-menu">
              <li id="totalnotif" class="header"></li>

              <li>
                <!-- inner menu: contains the actual data -->
                <ul id="listnotif" class="menu">
                 
                </ul>
              </li>
              <li id="footernotif" class="footer"></li>
            </ul>
          </li>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-user-circle-o "></i>
              <span class="hidden-xs"><?php echo $session['displayname']?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo site_url() ?>themes/admin/assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                <p>
                  <?php echo $session['displayname']?>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                    <a href="<?php echo site_url() ?>admin/userdetails/update/<?php echo $session['id']?>" class="btn btn-default btn-flat"><i class="fa fa-user-plus"></i> Profile</a>
                </div>
                <div class="pull-right">
                    <a href="<?php echo site_url() ?>admin/auth/logout" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Keluar</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->