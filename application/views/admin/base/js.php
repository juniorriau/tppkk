
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
        <b>Version</b> 0.1
        </div>
        <strong>Copyright &copy; 2018 - <?php echo date("Y");?> TP PKK Provinsi Riau. All rights
    reserved.
    </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- jQuery 3 -->
<script src="<?php echo site_url() ?>themes/admin/assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo site_url() ?>themes/admin/assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo site_url() ?>themes/admin/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="<?php echo site_url() ?>themes/admin/assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo site_url() ?>themes/admin/assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- datepicker -->
<script src="<?php echo site_url() ?>themes/admin/assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo site_url() ?>themes/admin/assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo site_url() ?>themes/admin/assets/bower_components/fastclick/lib/fastclick.js"></script>
<script src="<?php echo site_url() ?>themes/admin/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo site_url() ?>themes/admin/assets/plugins/clipboard/dist/clipboard.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo site_url() ?>themes/admin/assets/dist/js/adminlte.min.js"></script>

<script type="text/javascript">
    $('#dateadd').datepicker({
        format:'yyyy-mm-dd',
        autoclose: true
    });
    $('#datemodified').datepicker({
        format:'yyyy-mm-dd',
        autoclose: true
    });
    CKEDITOR.replace( 'content', {
        filebrowserBrowseUrl: '<?php echo site_url()?>themes/admin/assets/plugins/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
    });
    var clipboard = new Clipboard('.imgcopy');

    clipboard.on('success', function(e) {
        console.info('Action:', e.action);
        console.info('Text:', e.text);
        console.info('Trigger:', e.trigger);

        e.clearSelection();
    });

    clipboard.on('error', function(e) {
        console.error('Action:', e.action);
        console.error('Trigger:', e.trigger);
    });
</script>
    