  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo site_url() ?>themes/admin/assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $session['displayname']?></p>
        </div>
      </div>
      <!-- search form -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li><a href="<?php echo site_url() ?>admin"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li><a href="<?php echo site_url() ?>" target="blank_"><i class="fa fa-globe" ></i> <span>Visit Website</span></a></li>
            <li><a href="<?php echo site_url() ?>/admin/pages"><i class="fa fa-paper-plane-o"></i> <span>Pages</span></a></li>
            <li><a href="<?php echo site_url() ?>/admin/posts"><i class="fa fa-file-text"></i> <span>Posts</span></a></li>
            <li><a href="<?php echo site_url() ?>/admin/categories"><i class="fa fa-th"></i> <span>Categories</span></a></li>
            <li><a href="<?php echo site_url() ?>/admin/comments"><i class="fa fa-comment-o"></i> <span>Comments</span></a></li>
            <li><a href="<?php echo site_url() ?>/admin/mediafiles"><i class="fa fa-files-o"></i> <span>Media Files</span></a></li>
            <li><a href="<?php echo site_url() ?>/admin/menu"><i class="fa fa-th-list"></i> <span>Menu</span></a></li>
            <li><a href="<?php echo site_url() ?>/admin/users"><i class="fa fa-users"></i> <span>Users</span></a></li>
            <li><a href="<?php echo site_url() ?>/admin/settings"><i class="fa fa-gears"></i> <span>Settings</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
  </aside>