<?php
$this->load->view('admin/base/header');
$this->load->view('admin/base/css');
$this->load->view('admin/base/topbar');
$this->load->view('admin/base/sidebar');
?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 ">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $postcount;?><sup style="font-size: 20px"></sup></h3>

              <p>Total of Posts</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="<?php echo site_url('admin/posts');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 ">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $comcount;?><sup style="font-size: 20px"></sup></h3>

              <p>Total of Comments</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo site_url('admin/comments');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 ">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $visitcount;?></h3>

              <p>Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
          <div class="col-lg-4 ">
              <!-- PRODUCT LIST -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Recently Added Posts</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
                  <?php if(empty($recpost)){
                      
                  }else{
                      
                  }
                  ?>
                <!--<li class="item">
                  <div class="product-img">
                    <img src="<?php echo site_url()?>themes/admin/assets/dist/img/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <a href="javascript:void(0)" class="product-title">Samsung TV
                      <span class="label label-warning pull-right">$1800</span></a>
                    <span class="product-description">
                          Samsung 32" 1080p 60Hz LED Smart HDTV.
                        </span>
                  </div>
                </li>-->
                <!-- /.item -->
              </ul>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
          </div>
          <div class="col-lg-4 ">
              <!-- PRODUCT LIST -->
              <div class="box box-primary">
                  <div class="box-header with-border">
                      <h3 class="box-title">Comment Need to Approve</h3>

                      <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                          
                      </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                      <ul class="products-list product-list-in-box">
                            <?php if(empty($reccomment)){
                      
                            }else{

                            }
                            ?>                          
                          <!-- /.item 
                          <li class="item">
                              <div class="product-img">
                                  <img src="<?php echo site_url()?>themes/admin/assets/dist/img/default-50x50.gif" alt="Product Image">
                              </div>
                              <div class="product-info">
                                  <a href="javascript:void(0)" class="product-title">PlayStation 4
                                      <span class="label label-success pull-right">$399</span></a>
                                  <span class="product-description">
                                      PlayStation 4 500GB Console (PS4)
                                  </span>
                              </div>
                          </li>/.item -->
                      </ul>
                  </div>
                  <!-- /.box-footer -->
              </div>
              <!-- /.box -->
          </div>
          <div class="col-lg-4 ">
              <!-- PRODUCT LIST -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Recently Comment</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
                <?php if(empty($reccomment)){
                      
                            }else{

                            }
                            ?>                          
                          <!-- /.item 
                          <li class="item">
                              <div class="product-img">
                                  <img src="<?php echo site_url()?>themes/admin/assets/dist/img/default-50x50.gif" alt="Product Image">
                              </div>
                              <div class="product-info">
                                  <a href="javascript:void(0)" class="product-title">PlayStation 4
                                      <span class="label label-success pull-right">$399</span></a>
                                  <span class="product-description">
                                      PlayStation 4 500GB Console (PS4)
                                  </span>
                              </div>
                          </li>/.item -->
              </ul>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
          </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
<?php

$this->load->view('admin/base/js');
$this->load->view('admin/base/footer');
