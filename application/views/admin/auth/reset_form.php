<?php
$this->load->view('admin/base/header');
$this->load->view('admin/base/css');
?>
<div class="login-box">
  <div class="login-logo">
      <a href="<?php echo site_url();?>"><img src="<?php echo site_url();?>/themes/frontend/img/logo-tppkk.png" class="img img-responsive center-block"/></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Reset Password</p>

    <form action="<?php echo $action;?>" method="post">
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
      <div class="form-group has-feedback">
        <input type="email" name="email" id="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Forgot</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <!-- /.social-auth-links -->
  </div>
  <!-- /.login-box-body -->
</div>
<?php

$this->load->view('admin/base/footer');