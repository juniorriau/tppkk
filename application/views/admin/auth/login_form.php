<?php
$this->load->view('admin/base/header');
$this->load->view('admin/base/css');
?>
<div class="login-box">
  <div class="login-logo">
      <a href="<?php echo site_url();?>"><img width="50%" src="<?php echo site_url($this->config->item('site_logo'));?>" class="img img-responsive center-block"/></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <div class="alert alert-warning alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong><?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : 'Please Login First'; ?></strong>
      </div>
    <form action="<?php echo $action;?>" method="post">
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
      <div class="form-group has-feedback">
          <?php echo form_error('email') ?>
        <input type="email" name="email" id="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
          <?php echo form_error('password') ?>
          <input type="password" name="password" id="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <!-- /.social-auth-links -->

    <a href="<?php echo site_url("admin/auth/forgot")?>">I forgot my password</a><br>
  </div>
  <!-- /.login-box-body -->
</div>
<?php

$this->load->view('admin/base/footer');