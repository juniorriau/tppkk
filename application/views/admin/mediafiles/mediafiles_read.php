<?php
$this->load->view('admin/base/header');
$this->load->view('admin/base/css');
$this->load->view('admin/base/topbar');
$this->load->view('admin/base/sidebar');
?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo ucfirst($this->uri->segment(2));?>
        <small> panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li ><?php echo ucfirst($this->uri->segment(2));?></li>
        <li class="active"><?php echo ucfirst($this->uri->segment(3));?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
          <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-body">
        <table class="table">
	    <tr><td>Filename</td><td><?php echo $filename; ?></td></tr>
	    <tr><td>Filetype</td><td><?php echo $filetype; ?></td></tr>
	    <tr><td>Dateadd</td><td><?php echo $dateadd; ?></td></tr>
        <tr><td><img class="img-responsive img-thumbnail" src="<?php echo site_url($filepath)?>"/></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('admin/mediafiles') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
            </div>
            </div>
        </div>
    </div>
      <!-- /.row -->

      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
<?php

$this->load->view('admin/base/js');
$this->load->view('admin/base/footer');
