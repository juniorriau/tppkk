<?php
$this->load->view('admin/base/header');
$this->load->view('admin/base/css');
$this->load->view('admin/base/topbar');
$this->load->view('admin/base/sidebar');
?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo ucfirst($this->uri->segment(2));?>
        <small> panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li ><?php echo ucfirst($this->uri->segment(2));?></li>
        <li class="active"><?php echo ucfirst($this->uri->segment(3));?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
          <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
	    <div class="form-group">
            <label for="bigint">Userid <?php echo form_error('userid') ?></label>
            <input type="text" class="form-control" name="userid" id="userid" placeholder="Userid" value="<?php echo $userid; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Metakey <?php echo form_error('metakey') ?></label>
            <input type="text" class="form-control" name="metakey" id="metakey" placeholder="Metakey" value="<?php echo $metakey; ?>" />
        </div>
	    <div class="form-group">
            <label for="metavalue">Metavalue <?php echo form_error('metavalue') ?></label>
            <textarea class="form-control" rows="3" name="metavalue" id="metavalue" placeholder="Metavalue"><?php echo $metavalue; ?></textarea>
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('admin/userdetails') ?>" class="btn btn-default">Cancel</a>
	</form>
            </div>
            </div>
        </div>
    </div>
      <!-- /.row -->

      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
<?php

$this->load->view('admin/base/js');
$this->load->view('admin/base/footer');
