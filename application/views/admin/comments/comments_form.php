<?php
$this->load->view('admin/base/header');
$this->load->view('admin/base/css');
$this->load->view('admin/base/topbar');
$this->load->view('admin/base/sidebar');
?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo ucfirst($this->uri->segment(2));?>
        <small> panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li ><?php echo ucfirst($this->uri->segment(2));?></li>
        <li class="active"><?php echo ucfirst($this->uri->segment(3));?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
          <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
	    <div class="form-group">
            <label for="bigint">Postid <?php echo form_error('postid') ?></label>
            <input type="text" class="form-control" name="postid" id="postid" placeholder="Postid" value="<?php echo $postid; ?>" />
        </div>
	    <div class="form-group">
            <label for="comments">Comments <?php echo form_error('comments') ?></label>
            <textarea class="form-control" rows="3" name="comments" id="comments" placeholder="Comments"><?php echo $comments; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Commentstatus <?php echo form_error('commentstatus') ?></label>
            <input type="text" class="form-control" name="commentstatus" id="commentstatus" placeholder="Commentstatus" value="<?php echo $commentstatus; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Commentname <?php echo form_error('commentname') ?></label>
            <input type="text" class="form-control" name="commentname" id="commentname" placeholder="Commentname" value="<?php echo $commentname; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Commentemail <?php echo form_error('commentemail') ?></label>
            <input type="text" class="form-control" name="commentemail" id="commentemail" placeholder="Commentemail" value="<?php echo $commentemail; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Commenturl <?php echo form_error('commenturl') ?></label>
            <input type="text" class="form-control" name="commenturl" id="commenturl" placeholder="Commenturl" value="<?php echo $commenturl; ?>" />
        </div>
	    <div class="form-group">
            <label for="commentcontent">Commentcontent <?php echo form_error('commentcontent') ?></label>
            <textarea class="form-control" rows="3" name="commentcontent" id="commentcontent" placeholder="Commentcontent"><?php echo $commentcontent; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="datetime">Commentdate <?php echo form_error('commentdate') ?></label>
            <input type="text" class="form-control" name="commentdate" id="commentdate" placeholder="Commentdate" value="<?php echo $commentdate; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('admin/comments') ?>" class="btn btn-default">Cancel</a>
	</form>
            </div>
            </div>
        </div>
    </div>
      <!-- /.row -->

      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
<?php

$this->load->view('admin/base/js');
$this->load->view('admin/base/footer');
