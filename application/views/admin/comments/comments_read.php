<?php
$this->load->view('admin/base/header');
$this->load->view('admin/base/css');
$this->load->view('admin/base/topbar');
$this->load->view('admin/base/sidebar');
?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo ucfirst($this->uri->segment(2));?>
        <small> panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li ><?php echo ucfirst($this->uri->segment(2));?></li>
        <li class="active"><?php echo ucfirst($this->uri->segment(3));?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
          <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-body">
        <table class="table">
	    <tr><td>Postid</td><td><?php echo $postid; ?></td></tr>
	    <tr><td>Comments</td><td><?php echo $comments; ?></td></tr>
	    <tr><td>Commentstatus</td><td><?php echo $commentstatus; ?></td></tr>
	    <tr><td>Commentname</td><td><?php echo $commentname; ?></td></tr>
	    <tr><td>Commentemail</td><td><?php echo $commentemail; ?></td></tr>
	    <tr><td>Commenturl</td><td><?php echo $commenturl; ?></td></tr>
	    <tr><td>Commentcontent</td><td><?php echo $commentcontent; ?></td></tr>
	    <tr><td>Commentdate</td><td><?php echo $commentdate; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('comments') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
            </div>
            </div>
        </div>
    </div>
      <!-- /.row -->

      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
<?php

$this->load->view('admin/base/js');
$this->load->view('admin/base/footer');
