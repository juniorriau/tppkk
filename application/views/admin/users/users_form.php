<?php
$this->load->view('admin/base/header');
$this->load->view('admin/base/css');
$this->load->view('admin/base/topbar');
$this->load->view('admin/base/sidebar');
?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo ucfirst($this->uri->segment(2));?>
        <small> panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li ><?php echo ucfirst($this->uri->segment(2));?></li>
        <li class="active"><?php echo ucfirst($this->uri->segment(3));?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
          <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
	    <div class="form-group">
            <label for="varchar">Username <?php echo form_error('username') ?></label>
            <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Password <?php echo form_error('password'); ?> </label> <?php if($this->uri->segment(3)=="update"){ echo "<label class='label label-warning'>Fill to change the password</label>";}?>
            <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Useremail <?php echo form_error('useremail') ?></label>
            <input type="text" class="form-control" name="useremail" id="useremail" placeholder="Useremail" value="<?php echo $useremail; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Displayname <?php echo form_error('displayname') ?></label>
            <input type="text" class="form-control" name="displayname" id="displayname" placeholder="Displayname" value="<?php echo $displayname; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Userstatus <?php echo form_error('userstatus') ?></label>
            <select name="userstatus" id="userstatus" class="form-control">
                <option <?php echo $userstatus=="Admin"? 'selected':''?>>Admin</option>
                <option <?php echo $userstatus=="Editor"? 'selected':''?>>Editor</option>
            </select>
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('admin/users') ?>" class="btn btn-default">Cancel</a>
	</form>
            </div>
            </div>
        </div>
    </div>
      <!-- /.row -->

      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
<?php

$this->load->view('admin/base/js');
$this->load->view('admin/base/footer');
