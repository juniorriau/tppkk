<?php
$this->load->view('admin/base/header');
$this->load->view('admin/base/css');
$this->load->view('admin/base/topbar');
$this->load->view('admin/base/sidebar');
?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo ucfirst($this->uri->segment(2));?>
        <small> panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li ><?php echo ucfirst($this->uri->segment(2));?></li>
        <li class="active"><?php echo ucfirst($this->uri->segment(3));?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
          <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
            <input type="hidden" name="posttype" value="page"/>
	    <div class="form-group">
            <label for="title">Title <?php echo form_error('title') ?></label>
            <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?php echo $title; ?>"/>
            
        </div>
	    <div class="form-group">
            <label for="longtext">Content <?php echo form_error('content') ?></label>
            <textarea class="form-control" name="content" id="content"><?php echo $content; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Page Status <?php echo form_error('status') ?></label>
            <select name="status" id="status" class="form-control">
                <option> -- -- </option>
                <option <?php echo $status=="Draft"? 'selected':''?>>Draft</option>
                <option <?php echo $status=="Open"? 'selected':''?>>Open</option>
                <option <?php echo $status=="Closed"? 'selected':''?>>Closed</option>
            </select>
        </div>
	    <div class="form-group">
            <label for="varchar">PostImage <?php echo form_error('postimage') ?></label>
            <input type="file" class="form-control" name="postimage" id="postimage"/>
        </div>

	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('admin/pages') ?>" class="btn btn-default">Cancel</a>
	</form>
            </div>
            </div>
        </div>
    </div>
      <!-- /.row -->

      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
<?php

$this->load->view('admin/base/js');
$this->load->view('admin/base/footer');