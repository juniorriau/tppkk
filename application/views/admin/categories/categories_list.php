<?php
$this->load->view('admin/base/header');
$this->load->view('admin/base/css');
$this->load->view('admin/base/topbar');
$this->load->view('admin/base/sidebar');
?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo ucfirst($this->uri->segment(2));?>
        <small> panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li ><?php echo ucfirst($this->uri->segment(2));?></li>
        <li class="active"><?php echo ucfirst($this->uri->segment(3));?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
          <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-body">
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('admin/categories/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('admin/categories/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('admin/categories'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Title</th>
		<th>Permalink</th>
		<th>Action</th>
            </tr><?php
            foreach ($categories_data as $categories)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $categories->title ?></td>
			<td><?php echo $categories->permalink ?></td>
			<td style="text-align:center;">
                <a href="<?php echo site_url('admin/categories/read/'.$categories->id);?>" class="btn btn-default"><i class="fa fa-eye"></i></a> 
                <a href="<?php echo site_url('admin/categories/update/'.$categories->id)?>" class="btn btn-primary"><i class="fa fa-pencil-square"></i></a>
                <a href="<?php echo site_url('admin/categories/delete/'.$categories->id)?>" onclick="javasciprt: return confirm(\'Are You Sure ?\')" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
            </div>
            </div>
        </div>
    </div>
      <!-- /.row -->

      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
<?php

$this->load->view('admin/base/js');
$this->load->view('admin/base/footer');
